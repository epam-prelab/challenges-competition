package org.projects.epam.controller.documentation;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
            value = "0...N", defaultValue = "0"),
        @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Records per page", defaultValue = "20"),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                value = "Default sorting is asc")
})
public @interface ApiPageable {
}
