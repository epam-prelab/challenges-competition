package org.projects.epam.controller;

import org.projects.epam.controller.validation.ControllerValidationUtil;
import org.projects.epam.dto.registereduser.RegisteredUserPatchDTO;
import org.projects.epam.dto.registereduser.RegisteredUserReadDTO;
import org.projects.epam.dto.registereduser.RegisteredUserCreateDTO;
import org.projects.epam.security.JwtUtil;
import org.projects.epam.service.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
public class RegisteredUserController {

    @Autowired
    public RegisteredUserController(RegisteredUserService registeredUserService, JwtUtil jwtUtil) {
        this.registeredUserService = registeredUserService;
        this.jwtUtil = jwtUtil;
    }

    private final RegisteredUserService registeredUserService;
    private final JwtUtil jwtUtil;

    @PostMapping("/registration")
    public RegisteredUserReadDTO createRegisteredUser(@RequestBody @Valid RegisteredUserCreateDTO createDTO) {
        ControllerValidationUtil.validatePassword(createDTO.getPassword());
        ControllerValidationUtil.validateUsername(createDTO.getUsername());
        return registeredUserService.createRegisteredUser(createDTO);
    }

    @GetMapping("/me")
    public RegisteredUserReadDTO getRegisteredUser(@RequestHeader("Authorization") String jwt) {
        String userEmail = jwtUtil.getEmailFromToken(jwt);
        return registeredUserService.getRegisteredUser(userEmail);
    }

    @PatchMapping("/me")
    public RegisteredUserReadDTO patchRegisteredUser(@RequestHeader("Authorization") String jwt,
                                                     @RequestBody RegisteredUserPatchDTO patch) {
        String email = jwtUtil.getEmailFromToken(jwt);
        return registeredUserService.patchRegisteredUser(email, patch);
    }

}
