package org.projects.epam.controller;

import org.projects.epam.dto.challenge.UserChallengeCreateDTO;
import org.projects.epam.dto.challenge.UserChallengeDeleteDTO;
import org.projects.epam.dto.challenge.UserChallengePatchDTO;
import org.projects.epam.dto.challenge.UserChallengeReadDTO;
import org.projects.epam.security.JwtUtil;
import org.projects.epam.service.UserChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/challenges")
public class UserChallengeController {

    private final JwtUtil jwtUtil;
    private final UserChallengeService userChallengeService;

    @Autowired
    public UserChallengeController(JwtUtil jwtUtil, UserChallengeService userChallengeService) {
        this.jwtUtil = jwtUtil;
        this.userChallengeService = userChallengeService;
    }

    @GetMapping("/{id}")
    public UserChallengeReadDTO getUserChallenge(@PathVariable UUID id) {
        return userChallengeService.getUserChallenge(id);
    }

    @GetMapping
    public List<UserChallengeReadDTO> getUserChallenges(@RequestParam (defaultValue = "false") Boolean archived,
                                                        @RequestHeader("Authorization") String jwt) {
        String userEmail = jwtUtil.getEmailFromToken(jwt);
        return userChallengeService.getUserChallenges(archived, userEmail);
    }

    @PostMapping
    public UserChallengeReadDTO createUserChallenge(@RequestBody @Valid UserChallengeCreateDTO createDTO,
                                                @RequestHeader("Authorization") String jwt) {
        String userEmail = jwtUtil.getEmailFromToken(jwt);
        return userChallengeService.createUserChallenge(createDTO, userEmail);
    }

    @PatchMapping("/{id}")
    public UserChallengeReadDTO patchUserChallenge(@PathVariable UUID id, @RequestBody UserChallengePatchDTO patch) {
        return userChallengeService.patchUserChallenge(id, patch);
    }

    @DeleteMapping("/{id}")
    public UserChallengeDeleteDTO deleteUserChallenge(@PathVariable UUID id) {
        return userChallengeService.deleteUserChallenge(id);
    }
}
