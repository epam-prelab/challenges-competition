package org.projects.epam.controller.validation;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ControllerValidationUtil {

    public static void validatePassword(String password) {
        if (password.length() < 8) {
            throw new IllegalArgumentException("Password should contain at least 8 characters");
        }
    }

    public static void validateUsername(String username) {
        if (username.length() < 1 || username.length() > 30) {
            throw new IllegalArgumentException("Username should contain from 1 up to 30 characters");
        }
    }
}
