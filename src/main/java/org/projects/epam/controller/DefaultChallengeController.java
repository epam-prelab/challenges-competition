package org.projects.epam.controller;

import org.projects.epam.controller.documentation.ApiPageable;
import org.projects.epam.dto.PageResult;
import org.projects.epam.dto.challenge.UserChallengeReadDTO;
import org.projects.epam.dto.defaultchallenge.DefaultChallengeReadDTO;
import org.projects.epam.security.JwtUtil;
import org.projects.epam.service.DefaultChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/default-challenges")
public class DefaultChallengeController {

    private final JwtUtil jwtUtil;
    private final DefaultChallengeService defaultChallengeService;

    @Autowired
    public DefaultChallengeController(JwtUtil jwtUtil, DefaultChallengeService defaultChallengeService) {
        this.jwtUtil = jwtUtil;
        this.defaultChallengeService = defaultChallengeService;
    }

    @PostMapping("/{id}")
    public UserChallengeReadDTO createChallenge(@PathVariable UUID id, @RequestHeader("Authorization") String jwt) {
        String userEmail = jwtUtil.getEmailFromToken(jwt);
        return defaultChallengeService.createUserChallenge(id, userEmail);
    }

    @ApiPageable
    @GetMapping
    public PageResult<DefaultChallengeReadDTO> getAllDefaultChallenges(@ApiIgnore Pageable pageable){
        return defaultChallengeService.getAllDefaultChallenges(pageable);
    }
}
