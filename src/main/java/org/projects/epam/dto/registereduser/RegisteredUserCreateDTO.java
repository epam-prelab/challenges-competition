package org.projects.epam.dto.registereduser;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class RegisteredUserCreateDTO {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String username;
    @NotNull
    private String password;

}
