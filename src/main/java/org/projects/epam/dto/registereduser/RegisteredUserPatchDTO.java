package org.projects.epam.dto.registereduser;

import lombok.Data;

@Data
public class RegisteredUserPatchDTO {
    private String username;
    private String password;
}
