package org.projects.epam.dto.registereduser;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;


@Data
public class RegisteredUserReadDTO {
    private UUID id;
    private String email;
    private String username;

    private Instant createdAt;
    private Instant updatedAt;
}
