package org.projects.epam.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class AuthenticationResponseDTO {
    private String jwt;
    private String username;
    private UUID id;
}
