package org.projects.epam.dto.defaultchallenge;

import lombok.Data;

import java.util.UUID;

@Data
public class DefaultChallengeReadDTO {
    private UUID id;
    private String name;
    private Integer popularity;
    private Integer milestone;
}
