package org.projects.epam.dto.challenge;

import lombok.Data;

@Data
public class UserChallengeDeleteDTO {
    private String message;
}
