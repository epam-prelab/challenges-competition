package org.projects.epam.dto.challenge;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserChallengeCreateDTO {

    @NotNull
    private String name;

    private Integer milestone;
}
