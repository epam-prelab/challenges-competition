package org.projects.epam.dto.challenge;

import lombok.Data;
import org.projects.epam.domain.enumeration.ChallengeStatus;

import java.time.Instant;
import java.util.UUID;

@Data
public class UserChallengeReadDTO {
    private UUID id;
    private UUID registeredUserId;
    private ChallengeStatus status;
    private String name;
    private Boolean archived;
    private Integer milestone;
    private Instant startDate;
    private Instant lastAcceptDate;
    private Instant finishDate;
}
