package org.projects.epam.dto.challenge;

import lombok.Data;
import org.projects.epam.domain.enumeration.ChallengeStatus;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class UserChallengePatchDTO {
    private String name;
    private ChallengeStatus status;
    private Boolean archived;
    private Instant lastAcceptDate;
    private Instant finishDate;
}
