package org.projects.epam.service;

import org.projects.epam.security.AuthenticationResponse;
import org.projects.epam.security.TrackUserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    public AuthenticationResponse getAuthResponse(String jwt, TrackUserDetails userDetails) {
        AuthenticationResponse response = new AuthenticationResponse();
        response.setRegisteredUserId(userDetails.getId());
        response.setJwt(jwt);
        response.setUsername(userDetails.getUsername());
        return response;
    }
}
