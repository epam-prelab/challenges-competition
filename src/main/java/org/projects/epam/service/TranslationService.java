package org.projects.epam.service;

import org.projects.epam.domain.DefaultChallenge;
import org.projects.epam.domain.RegisteredUser;
import org.projects.epam.domain.UserChallenge;
import org.projects.epam.domain.enumeration.ChallengeStatus;
import org.projects.epam.dto.PageResult;
import org.projects.epam.dto.challenge.UserChallengeCreateDTO;
import org.projects.epam.dto.challenge.UserChallengePatchDTO;
import org.projects.epam.dto.challenge.UserChallengeReadDTO;
import org.projects.epam.dto.defaultchallenge.DefaultChallengeReadDTO;
import org.projects.epam.dto.registereduser.RegisteredUserCreateDTO;
import org.projects.epam.dto.registereduser.RegisteredUserPatchDTO;
import org.projects.epam.dto.registereduser.RegisteredUserReadDTO;
import org.projects.epam.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.stream.Collectors;

@Service
public class TranslationService {

    private final RepositoryHelper repositoryHelper;

    @Autowired
    public TranslationService(RepositoryHelper repositoryHelper) {
        this.repositoryHelper = repositoryHelper;
    }

    //toRead methods
    public RegisteredUserReadDTO toRead(RegisteredUser registeredUser) {
        RegisteredUserReadDTO dto = new RegisteredUserReadDTO();
        dto.setId(registeredUser.getId());
        dto.setEmail(registeredUser.getEmail());
        dto.setUsername(registeredUser.getUsername());
        dto.setCreatedAt(registeredUser.getCreatedAt());
        dto.setUpdatedAt(registeredUser.getUpdatedAt());
        return dto;
    }

    public UserChallengeReadDTO toRead(UserChallenge userChallenge) {
        UserChallengeReadDTO dto = new UserChallengeReadDTO();
        dto.setId(userChallenge.getId());
        dto.setName(userChallenge.getName());
        dto.setFinishDate(userChallenge.getFinishDate());
        dto.setMilestone(userChallenge.getMilestone());
        dto.setRegisteredUserId(userChallenge.getRegisteredUser().getId());
        dto.setStartDate(userChallenge.getStartDate());
        dto.setStatus(userChallenge.getStatus());
        dto.setArchived(userChallenge.getArchived());
        dto.setLastAcceptDate(userChallenge.getLastAcceptDate());
        return dto;
    }

    public DefaultChallengeReadDTO toRead(DefaultChallenge defaultChallenge){
        DefaultChallengeReadDTO dto = new DefaultChallengeReadDTO();
        dto.setId(defaultChallenge.getId());
        dto.setName(defaultChallenge.getName());
        dto.setMilestone(defaultChallenge.getMilestone());
        dto.setPopularity(defaultChallenge.getPopularity());
        return dto;
    }

    //toEntity methods
    public RegisteredUser toEntity(RegisteredUserCreateDTO create) {
        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setEmail(create.getEmail());
        registeredUser.setPassword(create.getPassword());
        registeredUser.setUsername(create.getUsername());
        return registeredUser;
    }

    public UserChallenge toEntity(UserChallengeCreateDTO create, String userEmail) {
        UserChallenge userChallenge = new UserChallenge();
        userChallenge.setMilestone(create.getMilestone());
        userChallenge.setName(create.getName());
        userChallenge.setStatus(ChallengeStatus.STARTED);
        userChallenge.setStartDate(Instant.now());
        userChallenge.setArchived(false);
        userChallenge.setLastAcceptDate(Instant.now());
        userChallenge.setRegisteredUser(repositoryHelper.getReferenceToUserIfExist(RegisteredUser.class, userEmail));
        return userChallenge;
    }

    public UserChallengeCreateDTO toEntity(DefaultChallenge defaultChallenge) {
        UserChallengeCreateDTO createDTO = new UserChallengeCreateDTO();
        createDTO.setName(defaultChallenge.getName());
        createDTO.setMilestone(defaultChallenge.getMilestone());
        return createDTO;
    }

    //patchEntity methods
    public void patchEntity(RegisteredUserPatchDTO patch, RegisteredUser registeredUser) {
        if (patch.getPassword() != null) {
            registeredUser.setPassword(patch.getPassword());
        }
        if (patch.getUsername() != null) {
            registeredUser.setUsername(patch.getUsername());
        }
    }
    public void patchEntity(UserChallengePatchDTO patch, UserChallenge userChallenge) {
        if (patch.getArchived() != null) {
            userChallenge.setArchived(patch.getArchived());
        }
        if (patch.getFinishDate() != null) {
            userChallenge.setFinishDate(patch.getFinishDate());
        }
        if (patch.getLastAcceptDate() != null) {
            userChallenge.setLastAcceptDate(patch.getLastAcceptDate());
        }
        if (patch.getName() != null) {
            userChallenge.setName(patch.getName());
        }
        if (patch.getStatus() != null) {
            userChallenge.setStatus(patch.getStatus());
        }
    }

    public PageResult<DefaultChallengeReadDTO> toPageResult(Page<DefaultChallenge> page) {
        PageResult<DefaultChallengeReadDTO> pageResult = new PageResult<>();
        pageResult.setData(page.getContent().stream().map(this::toRead).collect(Collectors.toList()));
        pageResult.setPage(page.getNumber());
        pageResult.setPageSize(page.getSize());
        pageResult.setTotalPages(page.getTotalPages());
        return pageResult;
    }

}
