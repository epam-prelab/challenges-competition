package org.projects.epam.service;

import org.projects.epam.domain.RegisteredUser;
import org.projects.epam.repository.RegisteredUserRepository;
import org.projects.epam.security.TrackUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TrackUserDetailsService implements UserDetailsService {

    @Autowired
    private RegisteredUserRepository userRepository;

    @Override
    public TrackUserDetails loadUserByUsername(String email) {
        Optional<RegisteredUser> user = userRepository.findByEmail(email);
        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + email));
        return user.map(TrackUserDetails::new).get();
    }

}
