package org.projects.epam.service;

import org.projects.epam.domain.DefaultChallenge;
import org.projects.epam.domain.UserChallenge;
import org.projects.epam.dto.PageResult;
import org.projects.epam.dto.challenge.UserChallengeCreateDTO;
import org.projects.epam.dto.challenge.UserChallengeReadDTO;
import org.projects.epam.dto.defaultchallenge.DefaultChallengeReadDTO;
import org.projects.epam.exception.EntityNotFoundException;
import org.projects.epam.repository.DefaultChallengeRepository;
import org.projects.epam.repository.UserChallengeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class DefaultChallengeService {

    private final TranslationService translationService;
    private final DefaultChallengeRepository defaultChallengeRepository;
    private final UserChallengeRepository userChallengeRepository;
    private final DefaultChallengeRepository defaultChallengePagination;

    @Autowired
    public DefaultChallengeService(TranslationService translationService,
                                   DefaultChallengeRepository defaultChallengeRepository,
                                   UserChallengeRepository userChallengeRepository,
                                   DefaultChallengeRepository defaultChallengePagination) {
        this.translationService = translationService;
        this.userChallengeRepository = userChallengeRepository;
        this.defaultChallengeRepository = defaultChallengeRepository;
        this.defaultChallengePagination = defaultChallengePagination;
    }

    public UserChallengeReadDTO createUserChallenge(UUID id, String userEmail) {
        DefaultChallenge defaultChallenge = defaultChallengeRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(DefaultChallenge.class, id));
        UserChallengeCreateDTO createDTO = translationService.toEntity(defaultChallenge);
        UserChallenge userChallenge = translationService.toEntity(createDTO, userEmail);
        userChallenge = userChallengeRepository.save(userChallenge);
        incrementDefaultChallengePopularity(defaultChallenge);
        return translationService.toRead(userChallenge);
    }

    public PageResult<DefaultChallengeReadDTO> getAllDefaultChallenges(Pageable pageable) {
        Page<DefaultChallenge> page = defaultChallengePagination.findAll(pageable);
        return translationService.toPageResult(page);
    }

    private void incrementDefaultChallengePopularity(DefaultChallenge defaultChallenge) {
        defaultChallenge.setPopularity(defaultChallenge.getPopularity() + 1);
        defaultChallengeRepository.save(defaultChallenge);
    }
}
