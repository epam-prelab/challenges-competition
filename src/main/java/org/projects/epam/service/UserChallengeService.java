package org.projects.epam.service;

import org.projects.epam.domain.RegisteredUser;
import org.projects.epam.domain.UserChallenge;
import org.projects.epam.dto.challenge.UserChallengeCreateDTO;
import org.projects.epam.dto.challenge.UserChallengeDeleteDTO;
import org.projects.epam.dto.challenge.UserChallengePatchDTO;
import org.projects.epam.dto.challenge.UserChallengeReadDTO;
import org.projects.epam.exception.EntityNotFoundException;
import org.projects.epam.repository.RepositoryHelper;
import org.projects.epam.repository.UserChallengeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserChallengeService {

    @Autowired
    public UserChallengeService(TranslationService translationService, RepositoryHelper repositoryHelper,
                                UserChallengeRepository userChallengeRepository) {
        this.translationService = translationService;
        this.repositoryHelper = repositoryHelper;
        this.userChallengeRepository = userChallengeRepository;
    }

    private final TranslationService translationService;
    private final RepositoryHelper repositoryHelper;
    private final UserChallengeRepository userChallengeRepository;

    public UserChallengeReadDTO createUserChallenge(UserChallengeCreateDTO createDTO, String userEmail) {
        UserChallenge userChallenge = translationService.toEntity(createDTO, userEmail);

        userChallenge = userChallengeRepository.save(userChallenge);
        return translationService.toRead(userChallenge);
    }

    public UserChallengeDeleteDTO deleteUserChallenge(UUID id) {
        userChallengeRepository.delete(repositoryHelper.getReferenceIfExist(UserChallenge.class, id));
        UserChallengeDeleteDTO deleteDTO = new UserChallengeDeleteDTO();
        deleteDTO.setMessage("deleted successfully");
        return deleteDTO;
    }

    public UserChallengeReadDTO getUserChallenge(UUID id) {
        UserChallenge userChallenge = userChallengeRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(UserChallenge.class, id));
        return translationService.toRead(userChallenge);
    }

    public List<UserChallengeReadDTO> getUserChallenges(Boolean archived, String userEmail) {
        RegisteredUser registeredUser = repositoryHelper.getReferenceToUserIfExist(RegisteredUser.class, userEmail);
        List<UserChallenge> userChallenges = userChallengeRepository
                .findByRegisteredUser_IdAndArchivedOrderByStartDateDesc(registeredUser.getId(), archived);
        return userChallenges.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public UserChallengeReadDTO patchUserChallenge(UUID id, UserChallengePatchDTO patch) {
        UserChallenge userChallenge = userChallengeRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(UserChallenge.class, id));
        translationService.patchEntity(patch, userChallenge);

        userChallenge = userChallengeRepository.save(userChallenge);
        return translationService.toRead(userChallenge);
    }
}
