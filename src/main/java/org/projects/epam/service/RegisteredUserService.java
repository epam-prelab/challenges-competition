package org.projects.epam.service;

import org.projects.epam.domain.RegisteredUser;
import org.projects.epam.dto.registereduser.RegisteredUserCreateDTO;
import org.projects.epam.dto.registereduser.RegisteredUserPatchDTO;
import org.projects.epam.dto.registereduser.RegisteredUserReadDTO;
import org.projects.epam.exception.EntityNotFoundException;
import org.projects.epam.repository.RegisteredUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;

@Service
public class RegisteredUserService {

    @Autowired
    public RegisteredUserService(RegisteredUserRepository registeredUserRepository, PasswordEncoder passwordEncoder,
                                 TranslationService translationService) {
        this.registeredUserRepository = registeredUserRepository;
        this.translationService = translationService;
        this.passwordEncoder = passwordEncoder;
    }

    private final RegisteredUserRepository registeredUserRepository;
    private final TranslationService translationService;
    private final PasswordEncoder passwordEncoder;

    public RegisteredUserReadDTO createRegisteredUser(RegisteredUserCreateDTO create) {
        RegisteredUser registeredUser = null;
        try {
            registeredUser = translationService.toEntity(create);
            String password = create.getPassword();
            registeredUser.setPassword(passwordEncoder.encode(password));
            registeredUser = registeredUserRepository.save(registeredUser);
        } catch (DataIntegrityViolationException ex) {
            throw new DataIntegrityViolationException("This email already exists");
        }
        return translationService.toRead(registeredUser);
    }

    public RegisteredUserReadDTO getRegisteredUser(String email) {
        RegisteredUser registeredUser = registeredUserRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
        return translationService.toRead(registeredUser);
    }

    public RegisteredUserReadDTO patchRegisteredUser(String email, RegisteredUserPatchDTO patch) {
        RegisteredUser registeredUser = registeredUserRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
        translationService.patchEntity(patch, registeredUser);
        try {
            registeredUser = registeredUserRepository.save(registeredUser);
        } catch (ConstraintViolationException ex) {
            throw new DataIntegrityViolationException("This email already exists");
        }
        return translationService.toRead(registeredUser);
    }

}
