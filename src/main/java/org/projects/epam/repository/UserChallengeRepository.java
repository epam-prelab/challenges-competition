package org.projects.epam.repository;

import org.projects.epam.domain.UserChallenge;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserChallengeRepository extends CrudRepository<UserChallenge, UUID> {
    List<UserChallenge> findByRegisteredUser_IdAndArchivedOrderByStartDateDesc(UUID registeredUserId, Boolean archived);
}
