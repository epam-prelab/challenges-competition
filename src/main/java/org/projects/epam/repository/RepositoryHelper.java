package org.projects.epam.repository;

import org.projects.epam.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.UUID;

@Component
public class RepositoryHelper {

    @PersistenceContext
    private EntityManager entityManager;

    public <E> E getReferenceIfExist(Class<E> entityClass, UUID id) {
        validateExist(entityClass, id);
        return entityManager.getReference(entityClass, id);
    }

    public <E> E getReferenceToUserIfExist(Class<E> entityClass, String userEmail) {
        UUID userId = getUserId(entityClass, userEmail);
        return entityManager.getReference(entityClass, userId);
    }

    public <E> void validateExist(Class<E> entityClass, UUID id) {
        Query query = entityManager.createQuery(
                "select count(e) from " + entityClass.getSimpleName() + " e where e.id = :id");
        query.setParameter("id", id);
        boolean exists = ((Number) query.getSingleResult()).intValue() > 0;
        if (!exists) {
            throw new EntityNotFoundException(entityClass, id);
        }
    }

    private <E> UUID getUserId(Class<E> entityClass, String userEmail) {
        Query query = entityManager.createQuery(
                "select e.id from " + entityClass.getSimpleName() + " e where e.email = :email");
        query.setParameter("email", userEmail);
        return (UUID) query.getSingleResult();
    }

}
