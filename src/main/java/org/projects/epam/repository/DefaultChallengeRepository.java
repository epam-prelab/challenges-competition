package org.projects.epam.repository;

import org.projects.epam.domain.DefaultChallenge;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface DefaultChallengeRepository extends PagingAndSortingRepository<DefaultChallenge, UUID> {

}
