package org.projects.epam.repository;

import org.projects.epam.domain.RegisteredUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RegisteredUserRepository extends CrudRepository<RegisteredUser, UUID> {
    Optional<RegisteredUser> findByEmail(String email);
}
