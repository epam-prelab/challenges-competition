package org.projects.epam.domain.enumeration;

public enum ChallengeStatus {
    STARTED,
    FAILED,
    DONE
}
