package org.projects.epam.domain;

import lombok.Getter;
import lombok.Setter;
import org.projects.epam.domain.enumeration.ChallengeStatus;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
public class UserChallenge {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser registeredUser;

    @Enumerated(EnumType.STRING)
    private ChallengeStatus status;

    private String name;
    private Integer milestone;
    private Boolean archived;

    @CreatedDate
    private Instant startDate;

    private Instant lastAcceptDate;
    private Instant finishDate;

}
