package org.projects.epam.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@Entity
public class DefaultChallenge {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    private String name;
    private Integer popularity;
    private Integer milestone;
}
