package org.projects.epam.security;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class AuthenticationResponse {
    private String jwt;
    private String username;
    private UUID registeredUserId;
}
